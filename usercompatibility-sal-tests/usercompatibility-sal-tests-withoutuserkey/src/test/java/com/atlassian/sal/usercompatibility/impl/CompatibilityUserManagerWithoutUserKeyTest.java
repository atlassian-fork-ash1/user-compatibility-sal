package com.atlassian.sal.usercompatibility.impl;

import java.net.URI;

import com.atlassian.sal.usercompatibility.UserKey;
import com.atlassian.sal.usercompatibility.UserManager;
import com.atlassian.sal.usercompatibility.UserProfile;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.sal.usercompatibility.UserKeys.getUserKeyFrom;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CompatibilityUserManagerWithoutUserKeyTest
{
    private final String USERNAME = "username";
    private final UserKey USERKEY = getUserKeyFrom(USERNAME);

    @Mock com.atlassian.sal.api.user.UserManager salUserManager;
    @Mock com.atlassian.sal.api.user.UserProfile salUserProfile;

    UserManager userManager;

    @Before
    public void setUp()
    {
        when(salUserProfile.getUsername()).thenReturn(USERNAME);
        when(salUserProfile.getFullName()).thenReturn("Full Name");
        when(salUserProfile.getEmail()).thenReturn("email@example.com");
        when(salUserProfile.getProfilePictureUri()).thenReturn(URI.create("/profile-pic"));
        when(salUserProfile.getProfilePageUri()).thenReturn(URI.create("/profile-page"));

        when(salUserManager.getUserProfile(USERNAME)).thenReturn(salUserProfile);
        when(salUserManager.getUserProfile(null)).thenReturn(null);
        when(salUserManager.getRemoteUsername()).thenReturn(USERNAME);
        when(salUserManager.isUserInGroup(eq(USERNAME), anyString())).thenReturn(true);
        when(salUserManager.isUserInGroup(isNull(String.class), anyString())).thenReturn(false);
        when(salUserManager.isAdmin(USERNAME)).thenReturn(true);
        when(salUserManager.isAdmin(null)).thenReturn(false);
        when(salUserManager.isSystemAdmin(USERNAME)).thenReturn(true);
        when(salUserManager.isSystemAdmin(null)).thenReturn(false);

        userManager = new CompatibilityUserManager(salUserManager);
    }

    @Test
    public void testGetRemoteUserKeyReturnsUsernameWhenSalUserKeyIsNotImplemented()
    {
        assertThat(userManager.getRemoteUserKey(), equalTo(USERKEY));
    }

    @Test
    public void verifyGetRemoteUserCallsSalRemoteUsernameAndGetUserProfileWhenSalUserKeyIsNotImplemented()
    {
        userManager.getRemoteUser();
        verify(salUserManager).getRemoteUsername();
        verify(salUserManager).getUserProfile(USERNAME);
    }

    @Test
    public void verifyGetRemoteUserKeyCallsSalRemoteUsernameWhenSalUserKeyIsNotImplemented()
    {
        userManager.getRemoteUserKey();
        verify(salUserManager).getRemoteUsername();
    }

    @Test
    public void verifyGetProfileCallsSalUserProfileByUsernameWhenSalUserKeyIsNotImplemented()
    {
        userManager.getUserProfile(USERKEY);
        verify(salUserManager).getUserProfile(USERNAME);
    }

    @Test
    public void verifyGetProfileByUsernameCallsSalUserProfile()
    {
        userManager.getUserProfileByUsername(USERNAME);
        verify(salUserManager).getUserProfile(USERNAME);
    }

    @Test
    public void verifyIsUserInGroupCallsSalIsUserInGroupWithUserKey()
    {
        userManager.isUserInGroup(USERKEY, "group");
        verify(salUserManager).isUserInGroup(USERNAME, "group");
    }

    @Test
    public void verifyIsAdminCallsSalIsAdmin()
    {
        userManager.isAdmin(USERKEY);
        verify(salUserManager).isAdmin(USERNAME);
    }

    @Test
    public void verifyIsSystemAdminCallsSalIsSystemAdmin()
    {
        userManager.isSystemAdmin(USERKEY);
        verify(salUserManager).isSystemAdmin(USERNAME);
    }

    @Test
    public void verifyAuthenticateCallsSalAuthenticate()
    {
        userManager.authenticate(USERNAME, USERNAME);
        verify(salUserManager).authenticate(USERNAME, USERNAME);
    }

    @Test
    public void testGetUserProfileReturnsCorrectProfileFromSalUserManager()
    {
        UserProfile profile = userManager.getUserProfile(USERKEY);
        assertThat(profile.getUserKey(), equalTo(USERKEY));
        assertThat(profile.getUsername(), equalTo(USERNAME));
        assertThat(profile.getEmail(), equalTo("email@example.com"));
        assertThat(profile.getFullName(), equalTo("Full Name"));
        assertThat(profile.getProfilePageUri(), equalTo(URI.create("/profile-page")));
        assertThat(profile.getProfilePictureUri(), equalTo(URI.create("/profile-pic")));
    }

    @Test
    public void testGetUserProfileForNullUserKey()
    {
        UserProfile profile = userManager.getUserProfile(null);
        assertThat(profile, is(nullValue()));
    }

    @Test
    public void testUserInGroupForNullUserKey()
    {
        assertFalse(userManager.isUserInGroup(null, "group"));
    }

    @Test
    public void testIsAdminForNullUserKey()
    {
        assertFalse(userManager.isAdmin(null));
    }

    @Test
    public void testIsSystemAdminForNullUserKey()
    {
        assertFalse(userManager.isSystemAdmin(null));
    }

}
