package com.atlassian.sal.usercompatibility;

import com.google.common.base.Function;

public class UserKeys
{
    private static Boolean userKeyImplemented;

    public static boolean isUserKeyImplemented()
    {
        if (userKeyImplemented == null)
        {
            try
            {
                getSalUserKeyClass();
                userKeyImplemented = true;
            }
            catch (CompatibilityUserUtilAccessException e)
            {
                userKeyImplemented = false;
            }
        }

        return userKeyImplemented;
    }

    public static Class<?> getSalUserKeyClass()
    {
        try
        {
            return Class.forName("com.atlassian.sal.api.user.UserKey");
        }
        catch (ClassNotFoundException e)
        {
            throw new CompatibilityUserUtilAccessException(e);
        }
    }

    public static UserKey getUserKeyFrom(String username)
    {
        if (username != null)
        {
            return new UserKey(IdentifierUtils.toLowerCase(username));
        }
        return null;
    }

    /**
     * Returns SAL's {@code UserKey}, if implemented. Returns a {@code String} otherwise.
     */
    public static Object getSalUserKeyOrStringFrom(String userKey)
    {
        if (userKey == null)
        {
            return null;
        }

        if (isUserKeyImplemented())
        {
            Class<?> userKeyClass = getSalUserKeyClass();
            try
            {
                return userKeyClass.getConstructor(String.class).newInstance(userKey);
            }
            catch (Exception e)
            {
                throw new CompatibilityUserUtilAccessException(e);
            }
        }
        // If UserKey is not implemented, treat userKey to be the same as username
        return userKey;
    }

    public static Function<String, UserKey> toUserKeys()
    {
        return new Function<String, UserKey>()
        {
            @Override
            public UserKey apply(String username)
            {
                return getUserKeyFrom(username);
            }
        };
    }
}
