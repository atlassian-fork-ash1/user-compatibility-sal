package com.atlassian.sal.usercompatibility.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.sal.api.user.UserResolutionException;
import com.atlassian.sal.usercompatibility.CompatibilityUserUtilAccessException;
import com.atlassian.sal.usercompatibility.UserKey;
import com.atlassian.sal.usercompatibility.UserManager;
import com.atlassian.sal.usercompatibility.UserProfile;

import static com.atlassian.sal.usercompatibility.UserKeys.getSalUserKeyClass;
import static com.atlassian.sal.usercompatibility.UserKeys.getUserKeyFrom;
import static com.atlassian.sal.usercompatibility.UserKeys.getSalUserKeyOrStringFrom;
import static com.atlassian.sal.usercompatibility.UserKeys.isUserKeyImplemented;
import static com.google.common.base.Preconditions.checkNotNull;

public class CompatibilityUserManager implements UserManager
{
    private final com.atlassian.sal.api.user.UserManager salUserManager;

    public CompatibilityUserManager(com.atlassian.sal.api.user.UserManager salUserManager)
    {
        this.salUserManager = checkNotNull(salUserManager, "salUserManager");
    }

    @Override
    public UserProfile getRemoteUser()
    {
        if (isUserKeyImplemented())
        {
            try
            {
                Method method = getUserManagerMethod("getRemoteUser");
                Object userProfileObject = method.invoke(salUserManager);
                if (userProfileObject != null)
                {
                    return new CompatibilityUserProfile((com.atlassian.sal.api.user.UserProfile) userProfileObject);
                }
            }
            catch (Exception e)
            {
                throw new CompatibilityUserUtilAccessException(e);
            }
        }

        return getUserProfileByUsername(salUserManager.getRemoteUsername());
    }

    @Override
    public UserKey getRemoteUserKey()
    {
        if (isUserKeyImplemented())
        {
            try
            {
                Method method = getUserManagerMethod("getRemoteUserKey");
                Object userKeyObject = method.invoke(salUserManager);
                if (userKeyObject != null)
                {
                    return new UserKey(userKeyObject.toString());
                }
            }
            catch (Exception e)
            {
                throw new CompatibilityUserUtilAccessException(e);
            }
        }
        return getUserKeyFrom(salUserManager.getRemoteUsername());
    }

    @Override
    public UserProfile getRemoteUser(HttpServletRequest request)
    {
        if (isUserKeyImplemented())
        {
            try
            {
                Method method = getUserManagerMethod("getRemoteUser", HttpServletRequest.class);
                Object userProfileObject = method.invoke(salUserManager, request);
                if (userProfileObject != null)
                {
                    return new CompatibilityUserProfile((com.atlassian.sal.api.user.UserProfile) userProfileObject);
                }
            }
            catch (Exception e)
            {
                throw new CompatibilityUserUtilAccessException(e);
            }
        }
        return getUserProfileByUsername(salUserManager.getRemoteUsername(request));
    }

    @Override
    public UserKey getRemoteUserKey(HttpServletRequest request)
    {
        if (isUserKeyImplemented())
        {
            try
            {
                Method method = getUserManagerMethod("getRemoteUserKey", HttpServletRequest.class);
                Object userKeyObject = method.invoke(salUserManager, request);
                if (userKeyObject != null)
                {
                    return new UserKey(userKeyObject.toString());
                }
            }
            catch (Exception e)
            {
                throw new CompatibilityUserUtilAccessException(e);
            }
        }
        return getUserKeyFrom(salUserManager.getRemoteUsername(request));
    }

    @Override
    public UserProfile getUserProfile(UserKey userKey)
    {
        try
        {
            Object userProfileObject = invokeMethodForUserKeyOrString("getUserProfile", username(userKey));
            if (userProfileObject != null)
            {
                return new CompatibilityUserProfile((com.atlassian.sal.api.user.UserProfile) userProfileObject);
            }
        }
        catch (Exception e)
        {
            throw new CompatibilityUserUtilAccessException(e);
        }
        return getUserProfileByUsername(username(userKey));
    }

    @Override
    public UserProfile getUserProfileByUsername(String username)
    {
        com.atlassian.sal.api.user.UserProfile salUserProfile = salUserManager.getUserProfile(username);
        if (salUserProfile != null)
        {
            return new CompatibilityUserProfile(salUserProfile);
        }
        return null;
    }

    @Override
    public boolean isUserInGroup(UserKey userKey, String group)
    {
        Object userKeyOrString = getSalUserKeyOrStringFrom(username(userKey));
        try
        {
            Method method = getUserManagerMethod("isUserInGroup", classFromUserKey(userKeyOrString), String.class);
            return Boolean.valueOf(method.invoke(salUserManager, userKeyOrString, group).toString());
        }
        catch (Exception e)
        {
            throw new CompatibilityUserUtilAccessException(e);
        }
    }

    @Override
    public boolean isSystemAdmin(UserKey userKey)
    {
        try
        {
            return Boolean.valueOf(invokeMethodForUserKeyOrString("isSystemAdmin", username(userKey)).toString());
        }
        catch (Exception e)
        {
            throw new CompatibilityUserUtilAccessException(e);
        }
    }

    @Override
    public boolean isAdmin(UserKey userKey)
    {
        try
        {
            return Boolean.valueOf(invokeMethodForUserKeyOrString("isAdmin", username(userKey)).toString());
        }
        catch (Exception e)
        {
            throw new CompatibilityUserUtilAccessException(e);
        }
    }

    @Override
    public boolean authenticate(String username, String password)
    {
        return salUserManager.authenticate(username, password);
    }

    @Override
    public Principal resolve(String username) throws UserResolutionException
    {
        return salUserManager.resolve(username);
    }

    private String username(UserKey userKey)
    {
        if (userKey != null)
        {
            return userKey.getStringValue();
        }
        return null;
    }

    private Method getUserManagerMethod(String methodName, Class<?>... paramClasses) throws NoSuchMethodException
    {
        Class<?> userManagerClass = com.atlassian.sal.api.user.UserManager.class;
        return userManagerClass.getMethod(methodName, paramClasses);
    }

    private Object invokeMethodForUserKeyOrString(String methodName, String userKey) throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException
    {
        Object userKeyOrString = getSalUserKeyOrStringFrom(userKey);
        Method method = getUserManagerMethod(methodName, classFromUserKey(userKeyOrString));
        return method.invoke(salUserManager, userKeyOrString);
    }

    private Class<?> classFromUserKey(Object userKey)
    {
        if (userKey == null)
        {
            return isUserKeyImplemented() ? getSalUserKeyClass() : String.class;
        }
        return userKey.getClass();
    }
}
